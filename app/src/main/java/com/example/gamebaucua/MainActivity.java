package com.example.gamebaucua;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
    public final static String EXTRA_MESSAGE = "com.example.gamebaucua.MESSAGE";
    final int firstCash = 200;
    Button btnCreate;
    EditText edtTen;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent = new Intent(this,PlayActivity.class);
        edtTen = (EditText) findViewById(R.id.edit_name);
        btnCreate = (Button) findViewById(R.id.btnCreate);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtTen.getText().toString().isEmpty()==false) {
                    intent.putExtra(EXTRA_MESSAGE, edtTen.getText() + "," + firstCash);
                    startActivity(intent);
                }
            }
        });
    }
}
