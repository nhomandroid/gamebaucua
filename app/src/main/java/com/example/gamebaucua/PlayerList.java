package com.example.gamebaucua;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class PlayerList extends Activity {
    ArrayList<Player> playerList = new ArrayList<>();

    public PlayerList() {
        super();
    }

    public PlayerList(ArrayList<Player> playerList) {
        this.playerList = playerList;
    }

    public ArrayList<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(ArrayList<Player> playerList) {
        this.playerList = playerList;
    }

    public void addPlayer(Player player) {
        this.playerList.add(player);
    }

    public void ghiObject(PlayerList playerList) {
        try {
            ObjectOutputStream objOut = new ObjectOutputStream
                    (openFileOutput("ListObject.txt", MODE_PRIVATE));
            objOut.writeObject(playerList);
            objOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PlayerList docObject(String fileName) {
        try {
            ObjectInputStream objIn = new ObjectInputStream
                    (openFileInput("ListObject.txt"));
            PlayerList playerList;
            playerList = (PlayerList) objIn.readObject();
            objIn.close();
            return playerList;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void ghiFile(PlayerList playerList) {
        try {
            PrintWriter wtrOut = new PrintWriter
                    (openFileOutput("ListWrite.txt", MODE_PRIVATE));
            for (Player player : playerList.getPlayerList()) {
                wtrOut.println(player);
            }
            wtrOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public PlayerList docFile() {
        try {
            Scanner wtrIn = new Scanner(openFileInput("ListWrite.txt"));
            PlayerList playerList = new PlayerList();
            while (wtrIn.hasNextLine()) {
                String line = wtrIn.nextLine();
                String[] nameCash = line.split(",");
                Player player = new Player();
                player.setName(nameCash[0]);
                player.setCash(Integer.parseInt(nameCash[1]));
                playerList.addPlayer(player);
            }
            wtrIn.close();
            return playerList;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void thongBao(String noiDung, Boolean exitOrNot) {
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle("Thông Báo!");
        msg.setMessage(noiDung);
        if (exitOrNot.booleanValue()) {
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }

    @Override
    public String toString() {
        return "{" + playerList + '}';
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}