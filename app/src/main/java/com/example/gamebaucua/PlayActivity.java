package com.example.gamebaucua;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class PlayActivity extends Activity {
    Button Play;
    ImageView h1, h2, h3;
    TextView name, cash;
    ArrayList<Integer> mang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        String[] nameCash = message.split(",");
        Play = (Button) findViewById(R.id.btnPlay);
        h1 = (ImageView) findViewById(R.id.image_ketQua1);
        h2 = (ImageView) findViewById(R.id.image_ketQua2);
        h3 = (ImageView) findViewById(R.id.image_ketQua3);
        name = (TextView) findViewById(R.id.text_name);
        cash = (TextView) findViewById(R.id.text_cash);
        name.setText("Player: "+nameCash[0]+" còn: ");
        cash.setText(nameCash[1]);
        mang = new ArrayList<Integer>();
        mang.add(R.drawable.nai);
        mang.add(R.drawable.bau);
        mang.add(R.drawable.ga);
        mang.add(R.drawable.ca);
        mang.add(R.drawable.cua);
        mang.add(R.drawable.tom);
        Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rd = new Random();
                int n = rd.nextInt(mang.size());
                h1.setImageResource(mang.get(n));

                int j = rd.nextInt(mang.size());
                h2.setImageResource(mang.get(j));

                int d = rd.nextInt(mang.size());
                h3.setImageResource(mang.get(d));
            }
        });
    }

    public void thongBao(String noiDung, Boolean exitOrNot){
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle("Thông Báo!");
        if (exitOrNot.booleanValue()){
            msg.setMessage(noiDung);
            msg.setPositiveButton("exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }else{
            msg.setMessage(noiDung);
            msg.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }msg.show();
    }
}
