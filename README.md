Phong với Thái làm tiếp phần đặt tiền nhé.

1. Khi gõ số tiền vào các con vật, tài khoản sẽ bị trừ đi ngay lúc đó.
2. Khi trúng con nào thì tài khoản được cộng 2 lần số tiền đã đặt con đó.
3. Trúng mấy con thì cứ nhân lên tiếp.
4. Hết tiền thì thông báo một câu và thoát game.
5. Ô nhập số tiền đặt không được nhập khác ngoài số.
6. Vô hiệu nút ENTER trong các EditText, vì khi nhấn ENTER nó xuống dòng và đẩy nội dung bên dưới xuống